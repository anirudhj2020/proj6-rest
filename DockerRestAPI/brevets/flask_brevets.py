"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
from pymongo import MongoClient
import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging


###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY


client = MongoClient('172.18.0.2', 27017)
db = client.brevetdb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    distance = request.args.get('distance', 1000, type = int)
    begin_date = request.args.get('begin_date', "2017-01-01", type = str)
    begin_time = request.args.get('begin_time', "00:00", type = str)
    time = arrow.get(begin_date + " " + begin_time).isoformat()
    open_time = acp_times.open_time(km, distance, time)
    close_time = acp_times.close_time(km, distance, time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)



@app.route("/_submit", methods = ["POST", "GET"])
def submit():
    db.brevetdb.drop()
    opn = request.form.get('open')
    close = request.form.get('close')
    item_doc = {'open':opn, 'close': close}
    db.brevetdb.insert_one(item_doc)
    return





@app.route("/_display", methods = ["POST"])
def display():
    _items = db.brevetdb.find()
    items = [item for item in _items]
    return flask.render_template('con_tm.html', items = items)




#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
