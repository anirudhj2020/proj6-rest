Project 6

Author: Anirudh Janardhan

Implementation of project 6. I wasn't able to get the api's to work however the docker compose should build correctly.


This software calculates open and close brevet times based on the official rules from RUSA (Randonneurs USA). We use min and max speeds to calculate close and open times respectively.

Control Distances    Min Speed    Max Speed

0-200 km        ---    15 km/hr    34 km/hr
200-400 km      ---    15 km/hr    32 km/hr
400-600 km      ---    15 km/hr    30 km/hr
600-1000 km     --- 11.428 km/hr   28 km/hr
1000-1300km     --- 13.333 km/hr   26 km/hr

